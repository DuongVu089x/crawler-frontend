import React, { useEffect, useState } from "react";
import styles from "../styles/Home.module.css";
import blockClient from "../api/blockClient.js";
import coinClient from "../api/coinClient.js";

const Home: React.FC = () => {
  return (
    <div className={styles.container}>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>Home work</h1>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Home;
