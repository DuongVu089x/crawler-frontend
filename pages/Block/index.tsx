import * as React from "react";

interface IBlockPageProps {}

const BlockPage: React.FunctionComponent<IBlockPageProps> = (props) => {
  return <h1>Block page work</h1>;
};

export default BlockPage;
