import * as React from 'react'
import Link from 'next/link'
import classes from './navbar.module.css'

interface INavbarProps {}

const Navbar: React.FunctionComponent<INavbarProps> = (props) => {
  return (
    <header className={classes.header}>
      <Link href="/">
        <a>Demo</a>
      </Link>
      <nav>
        <ul>
          <li>
            <Link href="/coin">Coin</Link>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Navbar
