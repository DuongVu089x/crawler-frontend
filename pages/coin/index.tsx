import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GetServerSideProps, GetStaticProps } from "next";
import { useRouter } from "next/router";

import Search from "components/search/search";
import coinClient from "api/blockClient";
import { addListCoin } from "store/coin/coin";
import { StyledButton } from "components/button/button";
import CoinList from "./components/coinList";

interface ICoinPageProps {
  coinList: any[];
  searchText: string;
  total: number;
  page: number;
  limit: number;
}

// export const getStaticProps: GetStaticProps = async (context) => {
//   console.log("getStaticProps");
//   let coinList = [];
//   try {
//     const params = {
//       q: JSON.stringify({
//         coin: {},
//         sortField: ["created_time"],
//       }),
//       offset: 0,
//       limit: 100,
//     };
//     const response = await coinClient.getAll(params);
//     coinList = response.data;
//   } catch (error) {
//     console.log(error);
//   } finally {
//     return {
//       props: {
//         coinList: coinList,
//       },
//     };
//   }
// };

export const getServerSideProps: GetServerSideProps = async (context) => {
  let limit = context.query.limit ? Number(context.query.limit) : 10;
  let offset = context.query.page
    ? (Number(context.query.page) - 1) * limit
    : 0;
  let searchText = context.query.searchText;

  let coinList = [];
  let total = 0;
  try {
    const params = {
      q: JSON.stringify({
        coin: {
          code: searchText,
        },
        sortField: ["created_time"],
      }),
      offset: offset,
      limit: limit,
      getTotal: true,
    };
    const response = await coinClient.getAll(params);
    if (response && response.data) {
      coinList = response.data;
      // @ts-ignore
      total = response.total; // Axios not found this field
    }
  } catch (error) {
    console.log(error);
  } finally {
    return {
      props: {
        coinList: coinList,
        searchText: searchText ? searchText : "",
        page: Number(context.query.page) ? Number(context.query.page) : 1,
        total: total,
        limit: limit,
      },
    };
  }
};

const CoinPage: React.FunctionComponent<ICoinPageProps> = ({
  coinList,
  searchText,
  total,
  page,
  limit,
}) => {
  const router = useRouter();
  const dispatch = useDispatch();

  // If the page is not yet generated, this will be displayed
  // initially until getStaticProps() finishes running
  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  useEffect(() => {
    const action = addListCoin({
      inputCoinList: coinList,
      inputSearchText: searchText,
      inputTotal: total,
      inputLimit: limit,
      inputPage: page,
    });
    dispatch(action);
  }, [coinList]);

  const excuteHandleChange = async (searchText: string) => {
    if (searchText.trim() == "") {
      router.replace(`/coin`);
    } else {
      router.replace(`/coin?searchText=${searchText}`);
    }

    // try {
    //   const params = {
    //     q: JSON.stringify({
    //       coin: {
    //         code: searchText.toUpperCase(),
    //       },
    //       sortField: ["created_time"],
    //     }),
    //     offset: 0,
    //     limit: limit,
    //   };
    //   const response = await coinClient.getAll(params);
    //   const action = addListCoin({
    //     inputCoinList: response.data,
    //     inputTotal: total,
    //     inputLimit: limit,
    //     inputPage: page,
    //   });
    //   dispatch(action);
    // } catch (error) {
    //   const action = addListCoin({
    //     inputCoinList: [],
    //     inputTotal: 0,
    //     inputLimit: limit,
    //     inputPage: 1,
    //   });
    //   dispatch(action);
    // }
  };

  const handleLoadDataClick = () => {
    coinClient.loadDataBatch();
    alert("Load data processing...");
  };

  return (
    <div>
      <h1>Coin page work</h1>
      <div className="display-flex-space-between">
        <div>
          <Search
            excuteHandleChange={excuteHandleChange}
            placeholder="Coin Code"
            value={searchText}
          />
        </div>
        <div className="mr-tb-auto">
          <StyledButton onClick={handleLoadDataClick}>Load data</StyledButton>
          {/* <button className="btn btn-primary"></button> */}
        </div>
      </div>
      <CoinList />
    </div>
  );
};

export default CoinPage;
