import axiosClient from './axiosClient'

const coinClient = {
  getAll: (params) => {
    const url = '/crawl-server/coin'
    return axiosClient.get(url, { params })
  },
}

export default coinClient
