import axiosClient from "./axiosClient";

const tradeClient = {
  getAll: (params) => {
    const url = "/crawl-server/trade";
    return axiosClient.get(url, { params });
  },
};

export default tradeClient;
