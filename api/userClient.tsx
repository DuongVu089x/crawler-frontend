import axiosClient, { responseData } from "./axiosClient";

const userClient = {
  register: (body) => {
    const url = "/crawl-server/user/register";
    return axiosClient.post(url, body);
  },
  checkEmail: (params) => {
    const url = "/crawl-server/user/check-email";
    return axiosClient.get(url, { params });
  },
};

export default userClient;
