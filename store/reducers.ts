import { combineReducers } from "redux";
import { coinReducer } from "./coin/coin";

export const rootReducer = combineReducers({
    coin: coinReducer
})

export type RootState = ReturnType<typeof rootReducer>
