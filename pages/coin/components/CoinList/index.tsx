import React from "react";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { coinSelector } from "store/coin/coin";
import Link from "next/link";
import DataGridComponent from "components/material/dataGrid";

type CoinListProps = {
  coinList?: any[];
};

const CoinList: React.FC<CoinListProps> = () => {
  const { coinList, total, limit, page } = useSelector(coinSelector);

  const router = useRouter();
  const columns: any[] = [
    { field: "code", headerName: "Coin", width: 150 },
    { field: "name", headerName: "Name", width: 150 },
    { field: "priceUsd", headerName: "Price USD", width: 150 },
    { field: "priceBtc", headerName: "Price Btc", width: 150 },
    { field: "priceEth", headerName: "Price Eth", width: 150 },
    { field: "change", headerName: "Change(%)", width: 150 },
    { field: "volume", headerName: "Volume", width: 150 },
    { field: "holders", headerName: "Holders", width: 150 },
  ];

  const rowsData = coinList.map((coin) => {
    return coin;
  });

  const excuteHandlePageChange = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    router.replace(`/coin?page=${newPage}`);
  };

  const excuteHandleDetail = (row: any) => {
    router.push(`/coin/${row.code}?page=1`);
  };

  const excuteHandleRemove = () => {};

  return (
    <DataGridComponent
      totalColumns={9}
      columnsTitle={columns}
      rowsData={rowsData}
      total={total}
      limit={limit}
      height={400}
      currentPage={
        page || (router.query?.page && Number(router.query.page)) || 1
      }
      excuteHandlePageChange={excuteHandlePageChange}
      excuteHandleDetail={excuteHandleDetail}
      excuteHandleRemove={excuteHandleRemove}
      isShowEdit={true}
      isShowDelete={false}
    />
    // <div className="scroll-table">
    //   <table className="table">
    //     <caption>List of coins</caption>
    //     <thead>
    //       <tr>
    //         <th scope="col">Code</th>
    //         <th scope="col">Name</th>
    //         <th scope="col">Price USD</th>
    //         <th scope="col">Price Btc</th>
    //         <th scope="col">Price Eth</th>
    //         <th scope="col">Change(%)</th>
    //         <th scope="col">Volume</th>
    //         <th scope="col">Holders</th>
    //         <th scope="col">Action</th>
    //       </tr>
    //     </thead>
    //     <tbody>
    //       {coinList.map((coin, index) => (
    //         <tr key={index}>
    //           <td>{coin.code}</td>
    //           <td>{coin.name}</td>
    //           <td>{coin.priceUsd}</td>
    //           <td>{coin.priceBtc}</td>
    //           <td>{coin.priceEth}</td>
    //           <td>{coin.change}</td>
    //           <td>{coin.volume}</td>
    //           <td>{coin.holders}</td>
    //           <td className="text-center">
    //             <Link href={`/coin/${coin.code}?page=1`}>
    //               <a>
    //                 <i className="fas fa-search"></i>
    //               </a>
    //             </Link>
    //           </td>
    //         </tr>
    //       ))}
    //     </tbody>
    //   </table>
    // </div>
  );
};
export default CoinList;
