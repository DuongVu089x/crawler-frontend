import React, { useEffect, useState } from "react";

type BlockListProps = {
  blockList?: any[];
};
const defaultProps: BlockListProps = {
  blockList: [],
};
const BlockList: React.FC<BlockListProps> = ({ blockList }) => {
  return (
    <tbody>
      {blockList.map((block) => (
        <tr key={block.blockCode}>
          <th scope="row">{block.blockCode}</th>
          <td>{block.blockCode}</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
      ))}
    </tbody>
  );
};
BlockList.defaultProps = defaultProps;
export default BlockList;
