import React, { useRef } from "react";
import { useForm } from "react-hook-form";
import Link from "next/link";

import {
  Container,
  CssBaseline,
  Avatar,
  Typography,
  Grid,
  TextField,
  FormControlLabel,
  Checkbox,
  Button,
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

import { REGEX_EMAIL, REGEX_PASSWORD } from "common/regex";
import userClient from "api/tradeClient";
import classes from "../account.module.css";

type FormInputs = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
  allowExtraEmails: boolean;
};

interface IRegisterPageProps {}

const RegisterPage: React.FunctionComponent<IRegisterPageProps> = (props) => {
  const {
    register,
    watch,
    formState: { errors, isValid },
    handleSubmit,
    setValue,
  } = useForm<FormInputs>({
    mode: "all",
  });

  const onSubmit = (data: any) => {};

  return (
    <>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  id="email"
                  name="email"
                  label="Email Address*"
                  variant="outlined"
                  autoComplete="email"
                  fullWidth
                  {...register("email", {
                    required: "Email is required",
                    pattern: {
                      value: REGEX_EMAIL,
                      message: "Email is not valid",
                    },
                  })}
                />
                {errors.email && (
                  <p className={classes.erros}>{errors.email.message}</p>
                )}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  id="password"
                  name="password"
                  type="password"
                  label="Password"
                  variant="outlined"
                  fullWidth
                  autoComplete="current-password"
                  {...register("password", {
                    required: "Password is required",
                    pattern: {
                      value: REGEX_PASSWORD,
                      message:
                        "Minimum eight characters, at least one letter, one number and one special character",
                    },
                  })}
                />
                {errors.password && (
                  <p className={classes.erros}>{errors.password.message}</p>
                )}
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={(classes.submit, "mt-16")}
              disabled={!isValid}
            >
              Sign In
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Link href={`/account/register`}>
                  <a className={classes.link}>Don't have an account? Sign up</a>
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
    </>
  );
};

export default RegisterPage;
