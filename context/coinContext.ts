import React from 'react'

const TradeContext = React.createContext({
    tradeList: [],
    page: 0,
    limit: 0,
    total: 0,
})

export const TradeProvider = TradeContext.Provider
export const TradeConsumer = TradeContext.Consumer
export default TradeContext