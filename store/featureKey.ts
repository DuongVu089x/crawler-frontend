/**
 * State feature key (prefix of action name)
 */
export const FeatureKey = {
    COIN : "COIN"
} as const
