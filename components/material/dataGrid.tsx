import React, { useEffect, useRef, useState } from "react";
import {
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from "@material-ui/core";
import { Pagination } from "@material-ui/lab";

import classes from "./dataGrid.module.css";
import Moment from "react-moment";

interface IDataGridComponentProps {
  rowsData?: any[];
  columnsTitle?: any[];
  totalColumns?: number;
  total?: number;

  currentPage?: number;
  limit?: number;

  width?: string;
  height?: number;

  isShowDelete?: boolean;
  isShowEdit?: boolean;

  excuteHandlePageChange?: (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => void;

  excuteHandleDetail?: (data: any) => void;
  excuteHandleRemove?: (data: any) => void;
}

const DataGridComponent: React.FunctionComponent<IDataGridComponentProps> = ({
  columnsTitle,
  totalColumns,
  rowsData,

  limit,
  total,
  currentPage,

  height,

  isShowDelete,
  isShowEdit,

  excuteHandlePageChange,
  excuteHandleDetail,
  excuteHandleRemove,
}) => {
  const [page, setPage] = useState(currentPage);
  const tableRef = useRef(null);

  useEffect(() => {
    // setPage(2);
    // Component did mount
    return () => {
      // Component will unmount
    };
  }, []);

  const handlePageChange = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    tableRef.current.scrollTop = 0;

    if (excuteHandlePageChange) {
      excuteHandlePageChange(event, newPage);
    }
  };

  const handleDetail: any = (data: any) => {
    console.log("handleDetail");
    if (excuteHandleDetail) {
      excuteHandleDetail(data);
    }
  };

  const handleRemove: any = (data: any) => {
    console.log("handleRemove");
    if (excuteHandleRemove) {
      excuteHandleRemove(data);
    }
  };

  return (
    <>
      <TableContainer
        component={Paper}
        className={classes.container}
        ref={tableRef} // Scroll to this element
        style={{ maxHeight: height }}
      >
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {columnsTitle.map((col, colIndex) => (
                <TableCell
                  key={colIndex}
                  align={col.align}
                  className={classes.tableHeaderCell}
                >
                  {col.headerName}
                </TableCell>
              ))}
              {(isShowDelete == true || isShowEdit == true) && (
                <TableCell className={classes.tableHeaderCell}></TableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
            {rowsData.length > 0 ? (
              rowsData.map((row, index) => (
                <TableRow key={index}>
                  {columnsTitle.map((col, colIndex) => (
                    <TableCell
                      key={colIndex}
                      align={col.align}
                      className={classes[col.className]}
                      style={{ maxWidth: col.width }}
                    >
                      {col.type == "date" ? (
                        <Moment format={col.dateFormat}>
                          {row[col.field]}
                        </Moment>
                      ) : col.type == "input" ? (
                        row[`${col.field}IsShow`] ? (
                          <TextField
                            id={`input-${col.field}-${index}`}
                            label={col.headerName}
                            defaultValue={row[col.field]}
                            variant="outlined"
                          />
                        ) : (
                          <TextField
                            id={`input-${col.field}-${index}`}
                            label={col.headerName}
                            defaultValue={row[col.field]}
                            variant="outlined"
                            disabled
                          />
                        )
                      ) : (
                        row[col.field]
                      )}
                    </TableCell>
                  ))}
                  {(isShowDelete == true || isShowEdit == true) && (
                    <TableCell style={{ minWidth: 80 }} align="center">
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        {isShowEdit &&
                          (row.isShowEdit == undefined ||
                            row.isShowEdit == true) && (
                            <IconButton
                              size="small"
                              onClick={() => handleDetail(row)}
                            >
                              <i className="fas fa-search"></i>
                            </IconButton>
                          )}
                        {isShowDelete &&
                          (row.isShowDelete == undefined ||
                            row.isShowDelete == true) && (
                            <IconButton
                              size="small"
                              onClick={() => handleRemove(row)}
                            >
                              <i className="fas fa-trash-alt"></i>
                            </IconButton>
                          )}
                      </div>
                    </TableCell>
                  )}
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell colSpan={totalColumns} align="center">
                  <p>Data not found</p>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <Pagination
        className={classes.pagination}
        count={Math.ceil(total / limit)}
        defaultPage={page}
        variant="outlined"
        shape="rounded"
        onChange={handlePageChange}
      />
    </>
  );
};

export default DataGridComponent;
