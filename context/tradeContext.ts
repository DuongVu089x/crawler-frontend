import React from 'react'

const CoinContext = React.createContext({})

export const UserProvider = CoinContext.Provider
export const UserConsumer = CoinContext.Consumer
export default CoinContext