import React from "react";
import { Provider } from "react-redux";
import Head from "next/head";
import { AppProps } from "next/app";
import { Router } from "next/router";
import NProgress from "nprogress";

import styles from "../styles/Home.module.css";
import "../styles/globals.css";
import Navbar from "components/navbar/navbar";
import store from "store/configureStore";

NProgress.configure({
  template: `<div>
  <div class="bar" role="bar">
    <div class="peg">
    </div>
  </div>
  <div class="spinner" role="spinner">
    <div class="spinner-icon">
    </div>
  </div>
  <div class="background">
  </div>
  </div>`,
});

Router.events.on("routeChangeStart", (url) => {
  console.log(`Loading: ${url}`);
  NProgress.start();
});
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

export default class Crawler extends React.Component<AppProps> {
  render() {
    return (
      <Provider store={store}>
        <div className="root-app">
          <Head>
            <title>Create Next App Edit</title>
            <link rel="icon" href="/favicon.ico" />
            <meta
              name="viewport"
              content="minimum-scale=1, initial-scale=1, width=device-width"
            />
          </Head>
          <Navbar />
          <div className={styles.container}>
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <this.props.Component {...this.props.pageProps} />
                </div>
              </div>
            </div>
          </div>
          <footer className={styles.footer}>
            <a
              href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
              target="_blank"
              rel="noopener noreferrer"
            >
              Powered by{" "}
              <img
                src="/vercel.svg"
                alt="Vercel Logo"
                className={styles.logo}
              />
            </a>
          </footer>
        </div>
      </Provider>
    );
  }
}
