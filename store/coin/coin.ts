import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { FeatureKey } from 'store/featureKey'
import { RootState } from 'store/reducers'

/**
 * Payload
 */
export type CoinPayload = {
  inputCoin?: any
  inputCoinList?: any[]
  inputSearchText?: string,
  inputTotal?: number,
  inputLimit?: number,
  inputPage?: number,
}

/**
 * State
 */
export type CoinState = {
  coinList: any[],
  searchText: string,
  total: number,
  limit: number,
  page: number,
}

const initialState: CoinState = {
  coinList: [],
  searchText: "",
  total: 0,
  limit: 0,
  page: 0,
}

const slice = createSlice({
  name: FeatureKey.COIN,
  initialState,
  reducers: {
    addListCoin(
      state: CoinState,
      action: PayloadAction<CoinPayload>,
    ) {
      const { payload } = action
      state.coinList = payload.inputCoinList
      state.searchText = payload.inputSearchText
      state.total = payload.inputTotal
      state.limit = payload.inputLimit
      state.page = payload.inputPage
    }
  },
})

/**
 * Reducer
 */
export const coinReducer = slice.reducer

/**
 * Action
 */
export const { addListCoin } = slice.actions

/**
 * Selector
 * @param state CounterState
 */
export const coinSelector = (state: RootState): CoinState => state.coin
