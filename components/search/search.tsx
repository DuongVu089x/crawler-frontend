import React, { useState, useEffect } from "react";

import classes from "./search.module.css";

interface ISearchComponentProps {
  excuteHandleChange: (searchText: string) => void;
  placeholder: string;
  value: string;
}

const Search: React.FunctionComponent<ISearchComponentProps> = ({
  excuteHandleChange,
  placeholder,
  value,
}) => {
  let searchTimeOut: NodeJS.Timeout;

  const handleChange = (event) => {
    const value = event.target.value;

    if (searchTimeOut) {
      clearTimeout(searchTimeOut);
    }
    searchTimeOut = setTimeout(() => {
      if (excuteHandleChange) {
        excuteHandleChange(value.trim());
      }
    }, 500);

    return () => {
      clearTimeout(searchTimeOut);
    };
  };

  return (
    <div className={classes.searchInput}>
      <input
        className={"form-control"}
        type="text"
        onChange={handleChange}
        placeholder={placeholder}
        value={value}
      />
    </div>
  );
};

export default Search;
