import React, { useContext } from "react";
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  IconButton,
} from "@material-ui/core";
import { Pagination } from "@material-ui/lab";
import Moment from "react-moment";

import TradeContext from "context/coinContext";
import classes from "./tradeList.module.css";

interface ITradeListProps {
  excuteHandlePageChange?: (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => void;

  excuteHandleDetail?: (data: any) => void;
  excuteHandleRemove?: (data: any) => void;
}

const TradeList: React.FunctionComponent<ITradeListProps> = ({
  excuteHandlePageChange,

  excuteHandleDetail,
  excuteHandleRemove,
}) => {
  const { tradeList, total, limit, page } = useContext(TradeContext);

  const columns: any[] = [
    { field: "coinCode", headerName: "Coin", width: 150 },
    {
      field: "txnHash",
      headerName: "Txn Hash",
      width: 150,
      className: "txnHash",
    },
    { field: "age", headerName: "Age", width: 150 },
    { field: "action", headerName: "Action", width: 150 },
    { field: "tokenAmountOut", headerName: "Token Amount (Out)", width: 150 },
    { field: "tokenAmountIn", headerName: "Token Amount (In)", width: 150 },
    { field: "swappedRate", headerName: "Swapped Rate", width: 150 },
    { field: "txnValue", headerName: "Txn Value ($)", width: 150 },
    {
      field: "actionBtn",
      headerName: "",
      width: 150,
      align: "center",
      // listAction: [
      //   <IconButton key="1" size="small">
      //     <i className="fas fa-search"></i>
      //   </IconButton>,
      //   <IconButton key="2" size="small">
      //     <i className="fas fa-trash-alt"></i>
      //   </IconButton>,
      // ],
    },
  ];

  const handlePageChange = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    if (excuteHandlePageChange) {
      excuteHandlePageChange(event, newPage);
    }
  };

  const handleDetail: any = (data: any) => {
    console.log("handleDetail");
    if (excuteHandleDetail) {
      excuteHandleDetail(data);
    }
  };

  const handleRemove: any = (data: any) => {
    console.log("handleRemove");
    if (excuteHandleRemove) {
      excuteHandleRemove(data);
    }
  };
  return (
    <>
      <TableContainer component={Paper} className={classes.container}>
        <Table stickyHeader className={classes.table} aria-label="">
          <TableHead>
            <TableRow>
              {columns.map((col, colIndex) => (
                <TableCell key={colIndex} align={col.align}>
                  {col.headerName}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {tradeList.map((row, index) => (
              <TableRow key={index}>
                <TableCell>{row.coinCode}</TableCell>
                <TableCell
                  className={classes.txnHash}
                  style={{ maxWidth: 150 }}
                >
                  {row.txnHash}
                </TableCell>
                <TableCell>
                  <Moment format="YYYY/MM/DD HH:mm:ss">{row.age}</Moment>
                </TableCell>
                <TableCell>{row.action}</TableCell>
                <TableCell>{row.tokenAmountOut}</TableCell>
                <TableCell>{row.tokenAmountIn}</TableCell>
                <TableCell>{row.swappedRate}</TableCell>
                <TableCell>{row.txnValue}</TableCell>
                <TableCell style={{ minWidth: 80 }} align="center">
                  <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <IconButton size="small" onClick={() => handleDetail(row)}>
                      <i className="fas fa-search"></i>
                    </IconButton>
                    <IconButton size="small" onClick={() => handleRemove(row)}>
                      <i className="fas fa-trash-alt"></i>
                    </IconButton>
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Pagination
        key={`pagination-${page}`} // To fix issue: A component is changing the default value state of an uncontrolled Slider after being initialized. =>https://stackoverflow.com/questions/62711040/a-component-is-changing-the-default-value-state-of-an-uncontrolled-slider-after
        className={classes.pagination}
        count={Math.ceil(total / limit)}
        defaultPage={page}
        variant="outlined"
        shape="rounded"
        onChange={handlePageChange}
      />
    </>
  );
};
// TradeList.defaultProps = defaultProps;
export default TradeList;
