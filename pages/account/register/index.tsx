import React, { useRef } from "react";
import { useForm } from "react-hook-form";
import NProgress from "nprogress";
import Link from "next/link";

import {
  Container,
  CssBaseline,
  Avatar,
  Typography,
  Grid,
  TextField,
  FormControlLabel,
  Checkbox,
  Button,
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

import { REGEX_EMAIL, REGEX_PASSWORD } from "common/regex";
import userClient from "api/userClient";
import classes from "../account.module.css";
import { useRouter } from "next/router";

type FormInputs = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
  allowExtraEmails: boolean;
};

interface IRegisterPageProps {}

const RegisterPage: React.FunctionComponent<IRegisterPageProps> = (props) => {
  const {
    register,
    watch,
    formState: { isValid, errors },
    handleSubmit,
    setValue,
    setError,
  } = useForm<FormInputs>({
    mode: "all",
  });

  const router = useRouter();

  const onSubmit = async (data: any) => {
    const registerResp = await userClient.register(data);
    console.log(registerResp);

    //@ts-ignore
    if (registerResp.status == "OK") {
      router.push("/account/login?regist=true");
      return;
    }
  };

  const onCheckEmailValid = async () => {
    // Case email is not valid pattern then return
    if (errors.email) {
      console.log(errors.email);
      return;
    }
    NProgress.start();
    try {
      const checkEmailResp = await userClient.checkEmail({
        email: watch("email"),
      });
      switch (checkEmailResp.status) {
        // @ts-ignore
        case "Error":
          setError("email", {
            type: "manual",
            message: "Error call api check email",
          });
          break;
        // @ts-ignore
        case "OK":
          setError("email", {
            type: "manual",
            message: "This email was regist before",
          });
          break;
      }
    } catch (error) {
      setError("email", {
        type: "manual",
        message: "Error call api check email",
      });
    }

    NProgress.done();
  };

  return (
    <>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  id="firstName"
                  variant="outlined"
                  label="First Name*"
                  name="firstName"
                  autoComplete="fname"
                  fullWidth
                  autoFocus
                  {...register("firstName", {
                    required: "Frist name is required",
                    maxLength: {
                      value: 20,
                      message: "Max length of frist name is 20",
                    },
                    minLength: {
                      value: 6,
                      message: "Min length of frist name is 6",
                    },
                  })}
                />
                {errors.firstName && (
                  <p className={classes.erros}>{errors.firstName.message}</p>
                )}
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  id="lastName"
                  name="lastName"
                  label="Last Name*"
                  variant="outlined"
                  autoComplete="lname"
                  fullWidth
                  {...register("lastName", {
                    required: "Last name is required",
                    maxLength: {
                      value: 20,
                      message: "Max length of frist name is 20",
                    },
                    minLength: {
                      value: 6,
                      message: "Min length of frist name is 6",
                    },
                  })}
                />
                {errors.lastName && (
                  <p className={classes.erros}>{errors.lastName.message}</p>
                )}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  id="email"
                  name="email"
                  label="Email Address*"
                  variant="outlined"
                  autoComplete="email"
                  fullWidth
                  {...register("email", {
                    required: "Email is required",
                    pattern: {
                      value: REGEX_EMAIL,
                      message: "Email is not valid",
                    },
                  })}
                  onBlur={onCheckEmailValid}
                />
                {errors.email && (
                  <p className={classes.erros}>{errors.email.message}</p>
                )}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  id="password"
                  name="password"
                  type="password"
                  label="Password"
                  variant="outlined"
                  fullWidth
                  autoComplete="current-password"
                  {...register("password", {
                    required: "Password is required",
                    pattern: {
                      value: REGEX_PASSWORD,
                      message:
                        "Minimum eight characters, at least one letter, one number and one special character",
                    },
                  })}
                />
                {errors.password && (
                  <p className={classes.erros}>{errors.password.message}</p>
                )}
              </Grid>
              <Grid item xs={12}>
                <TextField
                  id="confirmPassword"
                  name="confirmPassword"
                  type="password"
                  label="Cconfirm password"
                  variant="outlined"
                  fullWidth
                  autoComplete="confrirm-password"
                  {...register("confirmPassword", {
                    validate: (value) =>
                      value === watch("password") ||
                      "Password confirm not correct",
                  })}
                />
                {errors.confirmPassword && (
                  <p className={classes.erros}>
                    {errors.confirmPassword.message}
                  </p>
                )}
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox
                      color="primary"
                      onChange={(e) => {
                        setValue("allowExtraEmails", e.target.checked);
                      }}
                    />
                  }
                  {...register("allowExtraEmails")}
                  label="I want to receive inspiration, marketing promotions and updates via email."
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={!isValid}
            >
              Sign Up
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Link href={`/account/login`}>
                  <a className={classes.link}>
                    Already have an account? Sign in
                  </a>
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
    </>
  );
};

export default RegisterPage;
