import axiosClient from "./axiosClient";

const blockClient = {
  loadDataBatch: () => {
    const url = "/crawl-server/coin/batch";
    return axiosClient.get(url, {});
  },
  getAll: (params) => {
    const url = "/crawl-server/coin";
    return axiosClient.get(url, { params });
  },
};

export default blockClient;
