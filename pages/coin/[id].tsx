import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import { useRouter } from "next/router";
import Link from "next/link";
import { GetServerSideProps } from "next";

import { Button, IconButton, withStyles } from "@material-ui/core";

import { TradeProvider } from "context/coinContext";
import tradeClient from "api/tradeClient";
import classes from "./[id].module.css";
import TradeList from "./components/tradeList";
import DataGridComponent from "components/material/dataGrid";
import { StyledButton } from "components/button/button";
import { coinSelector } from "store/coin/coin";

interface ICoinDetailPageProps {
  coinCode: string;
  tradeList: any[];
  total: number;
  limit: number;
  page: number;
}

// export const getStaticProps: GetStaticProps = async (context) => {
//   console.log("getStaticProps");
//   console.log(context.params);
//   let tradeList = [];
//   let total = 0;
//   try {
//     const tradeParams = {
//       q: JSON.stringify({
//         trade: {
//           // coinCode: context.params.id,
//         },
//         sortField: ["-age"],
//       }),
//       offset: 0,
//       limit: 20,
//       getTotal: true,
//     };
//     const tradeResp = await tradeClient.getAll(tradeParams);
//     tradeList = tradeResp.data;
//     // @ts-ignore
//     total = tradeResp.total; // Axios not found this field
//   } catch (error) {
//     console.log(error);
//   } finally {
//     return {
//       props: {
//         tradeList: tradeList,
//         total: total,
//       },
//       revalidate: 5, // In seconds
//     };
//   }
// };

// export const getStaticPaths: GetStaticPaths = async () => {
//   console.log("getStaticPaths");
//   let coinList = [];
//   let paths;
//   try {
//     const params = {
//       q: JSON.stringify({
//         coin: {},
//         sortField: ["created_time"],
//       }),
//       offset: 0,
//       limit: 100,
//     };
//     const response = await coinClient.getAll(params);
//     coinList = response.data;
//   } catch (error) {
//   } finally {
//     // Get the paths we want to pre-render based on coins
//     if (coinList) {
//       paths = coinList.map((coin) => ({
//         params: { id: coin.code },
//       }));
//     }
//   }

//   return { paths, fallback: false };
// };

export const getServerSideProps: GetServerSideProps = async (context) => {
  let limit = context.query.limit ? Number(context.query.limit) : 10;
  let offset = context.query.page
    ? (Number(context.query.page) - 1) * limit
    : 0;

  let tradeList = [];
  let total = 0;
  try {
    const tradeParams = {
      q: JSON.stringify({
        trade: {
          coinCode: context.params.id,
        },
        sortField: ["-age"],
      }),
      offset: offset,
      limit: limit,
      getTotal: true,
    };
    const tradeResp = await tradeClient.getAll(tradeParams);
    tradeList = tradeResp.data;
    // @ts-ignore
    total = tradeResp.total; // Axios not found this field
  } catch (error) {
    console.log(error);
  } finally {
    return {
      props: {
        coinCode: context.params.id,
        tradeList: tradeList,
        page: Number(context.query.page),
        total: total,
        limit: limit,
      },
    };
  }
};

const CoinDetailPage: React.FunctionComponent<ICoinDetailPageProps> = ({
  coinCode,
  tradeList,
  total,
  page,
  limit,
}) => {
  const router = useRouter();
  const { query } = router;
  // const [tradeList, setTradeList] = useState(
  //   props.tradeList ? props.tradeList : []
  // );
  const coinState = useSelector(coinSelector);

  // If the page is not yet generated, this will be displayed
  // initially until getStaticProps() finishes running
  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  // Case same link, change path param or query param => Page not reloaded
  // Reason: when change path param | query param
  // => call api then sever side will return the new props
  // => but, in the client side, useState() only run once then not change state of page
  // => the data show in page not change
  // => use trick useEffect and dependency prop to listent data change and reset the state of page
  // useEffect(() => {
  //   setTradeList(props.tradeList);
  // }, [props.tradeList]);
  const columns: any[] = [
    { field: "coinCode", headerName: "Coin", width: 150 },
    {
      field: "txnHash",
      headerName: "Txn Hash",
      width: 150,
      className: "txnHash",
    },
    {
      field: "age",
      headerName: "Age",
      width: 150,
      type: "date",
      dateFormat: "YYYY/MM/DD HH:mm:ss",
    },
    {
      field: "action",
      headerName: "Action",
      width: 150,
      type: "input",
    },
    { field: "tokenAmountOut", headerName: "Token Amount (Out)", width: 150 },
    { field: "tokenAmountIn", headerName: "Token Amount (In)", width: 150 },
    { field: "swappedRate", headerName: "Swapped Rate", width: 150 },
    { field: "txnValue", headerName: "Txn Value ($)", width: 150 },
  ];

  const rowsData = tradeList.map((trade) => {
    if (trade.tokenAmountOut == 15000) {
      trade.actionIsShow = true;
      trade.isShowEdit = false;
      trade.isShowDelete = false;
    } else {
      trade.actionIsShow = false;
    }
    return trade;
  });

  const excuteHandlePageChange = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    router.replace(`/coin/${coinCode}?page=${newPage}`);
  };

  const excuteHandleDetail = (data: any) => {
    console.log(data);
  };

  const excuteHandleRemove = (data: any) => {
    console.log(data);
    alert(JSON.stringify(data));
  };

  const handleBackClick = () => {
    if (coinState.searchText == "") {
      router.push(`/coin?page=${coinState.page}`);
    } else {
      router.push(`/coin?searchText=${coinState.searchText}&page=1`);
    }
  };

  return (
    <>
      <div className={classes.header}>
        <h1>Coin detail {query.id}</h1>
        <StyledButton onClick={handleBackClick}>Back</StyledButton>
      </div>
      <TradeProvider value={{ tradeList, total, limit, page }}>
        <DataGridComponent
          totalColumns={9}
          columnsTitle={columns}
          rowsData={rowsData}
          total={total}
          limit={limit}
          height={500}
          currentPage={page}
          excuteHandlePageChange={excuteHandlePageChange}
          excuteHandleDetail={excuteHandleDetail}
          excuteHandleRemove={excuteHandleRemove}
          isShowDelete={true}
          isShowEdit={true}
        />
        {/* <TradeList
          excuteHandlePageChange={excuteHandlePageChange}
          excuteHandleDetail={excuteHandleDetail}
          excuteHandleRemove={excuteHandleRemove}
        /> */}
      </TradeProvider>
    </>
  );
};

export default CoinDetailPage;
