import { Button, withStyles } from "@material-ui/core";

// Custom style for material element
export const StyledButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    margin: "auto 0",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);
